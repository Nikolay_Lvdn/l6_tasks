﻿using System;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using ProjectStructure.Business.DTO;
using ProjectStructure.Business.Interfaces;
using ProjectStructure.Business.Services;
using ProjectStructure.DataAccess;
using ProjectStructure.DataAccess.Interfaces;
using ProjectStructure.DataAccess.Repositories;
using ProjectStructure.WebAPI.Mapping;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xunit;
using ProjectStructure.DataAccess.Entities;
using FakeItEasy;

namespace ProjectStruture.Business.Tests
{
    public class ProjectServiceTests : IDisposable
    {
        private readonly IProjectService _projectService;
        private List<Project> projects = new List<Project>();
        private List<Task> tasks = new List<Task>();
        private List<User> users = new List<User>();

        public ProjectServiceTests()
        {
            var mappingConfig = new MapperConfiguration(mc =>
            {
                mc.AddProfile(new MappingProfile());
            });

            IMapper mapper = mappingConfig.CreateMapper();

            var _unitOfWork = A.Fake<IUnitOfWork>();
            var _projectRepository = A.Fake<IProjectRepository>();
            var _taskRepository = A.Fake<ITaskRepository>();
            var _userRepository = A.Fake<IUserRepository>();

            A.CallTo(() => _unitOfWork.GetProjectRepository).Returns(_projectRepository);
            A.CallTo(() => _unitOfWork.GetTaskRepository).Returns(_taskRepository);
            A.CallTo(() => _unitOfWork.GetUserRepository).Returns(_userRepository);

            A.CallTo(() => _projectRepository.GetAsync()).Returns(projects);
            A.CallTo(() => _taskRepository.GetAsync()).Returns(tasks);
            A.CallTo(() => _userRepository.GetAsync()).Returns(users);

            _projectService = new ProjectService(_unitOfWork, mapper);
        }

        public void Dispose()
        {
            projects.Clear();
            tasks.Clear();
            users.Clear();
        }

        [Fact]
        public async void Query1_WhenNoData_ThenThrowArgumentNullException()
        {
            await Assert.ThrowsAsync<ArgumentNullException>(() => _projectService.GetQuery1(1));
        }

        [Fact]
        public async void Query1_WhenProjectWith1Task_ThenSuccess()
        {
            users.Add(new User { Id = 1 });
            tasks.Add(new Task { ProjectId = 1 });
            projects.Add(new Project { Id = 1, AuthorId = 1, Tasks = tasks });


            var query1 = await _projectService.GetQuery1(1);

            Assert.Single(query1);
            Assert.Equal(projects[0].Id, query1[0].Project.Id);
            Assert.Equal(tasks.Count, query1[0].Count);
        }

        [Fact]
        public async void Query7_WhenNoData_ThenThrowArgumentNullException()
        {
            await Assert.ThrowsAsync<ArgumentNullException>(() => _projectService.GetQuery7());
        }

        [Fact]
        public async void Query7_WhenProjectWith0Tasks_ThenSuccess()
        {
            projects.Add(new Project());

            Assert.Single(await _projectService.GetQuery7());
        }

        [Fact]
        public async void Query7_WhenProjectWith3TasksAndDescriptionMoreThen20_ThenSuccess()
        {
            projects.Add(new Project { Id = 1, Description = "---------------------" });
            tasks.Add(new Task { ProjectId = 1 });
            tasks.Add(new Task { ProjectId = 1 });
            tasks.Add(new Task { ProjectId = 1 });

            Assert.Single(await _projectService.GetQuery7());
        }

        [Fact]
        public async void Query7_WhenProjectWith3TasksAndDescriptionLessThen20_ThenThrowArgumentNullException()
        {
            tasks.Add(new Task { ProjectId = 1 });
            tasks.Add(new Task { ProjectId = 1 });
            tasks.Add(new Task { ProjectId = 1 });

            projects.Add(new Project { Id = 1, Description = "-", Tasks = tasks });


            await Assert.ThrowsAsync<ArgumentNullException>(() => _projectService.GetQuery7());
        }

        [Fact]
        public async void Query7_WhenProjectWithCorrectData_ThenSuccess()
        {
            tasks.Add(new Task { Id = 1, ProjectId = 1, Name = "--", Description = "--" });
            tasks.Add(new Task { Id = 2, ProjectId = 1, Name = "-", Description = "-" });

            projects.Add(new Project { Id = 1, Tasks = tasks });

            var query7 = await _projectService.GetQuery7();

            Assert.Single(query7);
            Assert.Equal(projects[0].Id, query7[0].Project.Id);
            Assert.Equal(tasks[0].Id, query7[0].LongestTask.Id);
            Assert.Equal(tasks[1].Id, query7[0].ShortestTask.Id);
            Assert.Null(query7[0].CountOfMembers);
        }
    }
}
