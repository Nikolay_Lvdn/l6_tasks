﻿using AutoMapper;
using ProjectStructure.Business.DTO;
using ProjectStructure.Business.Interfaces;
using ProjectStructure.DataAccess.Entities;
using ProjectStructure.DataAccess.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;
using Task = System.Threading.Tasks.Task;
using TaskEntity = ProjectStructure.DataAccess.Entities.Task;

namespace ProjectStructure.Business.Services
{
    public class TaskService : ITaskService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        public TaskService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task CreateAsync(TaskDTO taskDTO)
        {
            if (taskDTO == null)
                throw new Exception("Wrong request body");

            if ((await _unitOfWork.GetTaskRepository.GetAsync()).Select(task => task.Name).Contains(taskDTO.Name))
                throw new ArgumentException("Such task is already exists");

            var taskEntity = _mapper.Map<TaskEntity>(taskDTO);

            taskEntity.CreatedAt = DateTime.Now;

            await _unitOfWork.GetTaskRepository.CreateAsync(taskEntity);
            await _unitOfWork.SaveChangesAsync();
        }

        public async Task<IList<TaskDTO>> GetAllAsync()
        {
            return _mapper.Map<IList<TaskEntity>, IList<TaskDTO>>(await _unitOfWork.GetTaskRepository.GetAsync());
        }

        public async Task UpdateAsync(TaskDTO taskDTO)
        {
            var taskToUpdate = (await _unitOfWork.GetTaskRepository.GetAsync()).FirstOrDefault(task => task.Id == taskDTO.Id);

            if (taskToUpdate == null)
                throw new ArgumentNullException("No such Task");

            if ((await _unitOfWork.GetTaskRepository.GetAsync())
                .Where(task => task.Id != taskToUpdate.Id)
                .Select(task => task.Name)
                .Contains(taskDTO.Name))
                throw new ArgumentException("Such task is already exists");

            taskToUpdate.ProjectId = taskDTO.ProjectId;
            taskToUpdate.PerformerId = taskDTO.PerformerId;
            taskToUpdate.Name = taskDTO.Name;
            taskToUpdate.Description = taskDTO.Description;
            taskToUpdate.State = taskDTO.State;
            taskToUpdate.FinishedAt = taskDTO.FinishedAt;
            await _unitOfWork.GetTaskRepository.UpdateAsync(taskToUpdate);
            await _unitOfWork.SaveChangesAsync();
        }

        public async Task DeleteAsync(int id)
        {
            var taskToDelete = (await _unitOfWork.GetTaskRepository.GetAsync()).FirstOrDefault(task => task.Id == id);

            if (taskToDelete == null)
                throw new Exception("No such Task");

            await _unitOfWork.GetTaskRepository.DeleteAsync(id);
            await _unitOfWork.SaveChangesAsync();
        }

        public async Task<IList<TaskDTO>> GetQuery2(int id)
        {
            if (!(await _unitOfWork.GetUserRepository.GetAsync()).Select(user => user.Id).Contains(id))
                throw new ArgumentNullException("There are no such user");

            var tasks = await _unitOfWork.GetTaskRepository.GetAsync();

            List<TaskEntity> result = tasks.Where(task => task.PerformerId == id && task.Name?.Length < 45).ToList();

            if (!result.Any())
                throw new ArgumentNullException("There are no such tasks");

            return _mapper.Map<IList<TaskEntity>, IList<TaskDTO>>(result);
        }

        public async Task<IList<Query3DTO>> GetQuery3(int id)
        {
            if (!(await _unitOfWork.GetUserRepository.GetAsync()).Select(user => user.Id).Contains(id))
                throw new ArgumentNullException("There are no such user");

            var tasks = (await _unitOfWork.GetTaskRepository.GetAsync())
                 .Where(task => task.PerformerId == id
                 && task.FinishedAt != null
                 && task.FinishedAt.Value.Year == DateTime.Now.Year);

            if (!tasks.Any())
                throw new ArgumentNullException("There are no such tasks");

            return await Task.WhenAll(tasks.Select(MakeQuery3Selection));
        }

        private async Task<Query3DTO> MakeQuery3Selection(TaskEntity task)
        {
            return await Task.Run(() =>
            {
                return new Query3DTO()
                {
                    Id = task.Id,
                    Name = task.Name
                };
            });
        }

        public async Task<IList<TaskDTO>> GetQuery8(int id)
        {
            if (!(await _unitOfWork.GetUserRepository.GetAsync()).Select(user => user.Id).Contains(id))
                throw new ArgumentNullException("There are no such user");

            var tasks = await _unitOfWork.GetTaskRepository.GetAsync();

            List<TaskEntity> result = tasks.Where(task => task.PerformerId == id && task.State != 2 && task.State != 3).ToList();

            if (!result.Any())
                throw new ArgumentNullException("There are no such tasks");

            return _mapper.Map<IList<TaskEntity>, IList<TaskDTO>>(result);
        }

    }
}
