﻿using ProjectStructure.Business.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectStructure.Business.Interfaces
{
    public interface IUserService
    {
        Task CreateAsync(UserDTO userDTO);
        Task<IList<UserDTO>> GetAllAsync();
        Task UpdateAsync(UserDTO userDTO);
        Task DeleteAsync(int id);
        Task<IList<Query5DTO>> GetQuery5();
        Task<Query6DTO> GetQuery6(int id);
    }
}
