﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using ProjectStructure.Business.DTO;
using ProjectStructure.Business.Interfaces;
using ProjectStructure.WebAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace ProjectStructure.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TeamController : ControllerBase
    {
        private readonly ITeamService _teamService;
        private readonly IMapper _mapper;

        public TeamController(ITeamService teamService, IMapper mapper)
        {
            _teamService = teamService;
            _mapper = mapper;
        }

        // GET: api/<TeamController>
        [HttpGet]
        public async Task<IActionResult> GetAsync()
        {
            var teamsDTO = await _teamService.GetAllAsync();
            var teamsView = _mapper.Map<IList<TeamDTO>, IList<GettingTeamView>>(teamsDTO);

            return Ok(teamsView);
        }


        // POST api/<TeamController>
        [HttpPost]
        public async Task<IActionResult> PostAsync([FromBody] TeamView teamView)
        {
            try
            {
                var teamDTO = _mapper.Map<TeamView, TeamDTO>(teamView);

                await _teamService.CreateAsync(teamDTO);

                return CreatedAtAction("POST", teamView);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        // PUT api/<TeamController>/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutAsync(int id, [FromBody] TeamView teamView)
        {
            try
            {
                var teamDTO = _mapper.Map<TeamView, TeamDTO>(teamView);
                teamDTO.Id = id;
                await _teamService.UpdateAsync(teamDTO);

                return Ok();
            }
            catch(Exception ex)
            {
                return NotFound(ex.Message);
            }
        }

        // DELETE api/<TeamController>/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteAsync(int id)
        {
            try
            {
                await _teamService.DeleteAsync(id);
                return NoContent();
            }
            catch (Exception ex)
            {
                return NotFound(ex.Message);
            }
        }

        // GET: api/<TeamController>
        [HttpGet("q4")]
        public async Task<IActionResult> GetQuery4()
        {
            try
            {
                var result = _mapper.Map<IList<Query4DTO>, IList<Query4View>>(await _teamService.GetQuery4());

                return Ok(result);
            }
            catch(Exception ex)
            {
                return NotFound(ex.Message);
            }
        }
    }
}
