﻿using ProjectStructure.DataAccess;
using ProjectStructure.DataAccess.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using ProjectStructure.Show;
using System.Timers;

namespace ProjectStructure.Program
{
    class Program
    {

        static async System.Threading.Tasks.Task Main(string[] args)
        {
            while (true)
            {
                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.WriteLine("Enter command(? - to call /help):");
                Console.ForegroundColor = ConsoleColor.White;

                string input = Console.ReadLine();
                try
                {
                    Console.WriteLine();
                    switch (input)
                    {
                        case "1":
                            ShowUI.GetCountOfTasksOfProjectsByUserId(ShowUI.GetIdFromUser());
                            break;
                        case "2":
                            ShowUI.GetTasksByUserIdWithShortName(ShowUI.GetIdFromUser());
                            break;
                        case "3":
                            ShowUI.GetFinishedTaskByUserIdInThisYear(ShowUI.GetIdFromUser());
                            break;
                        case "4":
                            ShowUI.GetTeamsWithUsersOlderThan9();
                            break;
                        case "5":
                            ShowUI.GetOrderedUsersAndTasks();
                            break;
                        case "6":
                            ShowUI.GetInfoAboutTasksByUserId(ShowUI.GetIdFromUser());
                            break;
                        case "7":
                            ShowUI.GetInfoAboutProjects();
                            break;
                        case "8":
                            ShowUI.GetProjects();
                            break;
                        case "9":
                            ShowUI.GetTasks();
                            break;
                        case "10":
                            ShowUI.GetTeams();
                            break;
                        case "11":
                            ShowUI.GetUsers();
                            break;
                        case "12":
                            ShowUI.DeleteProject(ShowUI.GetIdFromUser());
                            break;
                        case "13":
                            ShowUI.DeleteTask(ShowUI.GetIdFromUser());
                            break;
                        case "14":
                            ShowUI.DeleteTeam(ShowUI.GetIdFromUser());
                            break;
                        case "15":
                            ShowUI.DeleteUser(ShowUI.GetIdFromUser());
                            break;
                        case "16":
                            ShowUI.PostProject();
                            break;
                        case "17":
                            ShowUI.PostTask();
                            break;
                        case "18":
                            ShowUI.PostTeam();
                            break;
                        case "19":
                            ShowUI.PostUser();
                            break;
                        case "20":
                            ShowUI.PutProject(ShowUI.GetIdFromUser());
                            break;
                        case "21":
                            ShowUI.PutTask(ShowUI.GetIdFromUser());
                            break;
                        case "22":
                            ShowUI.PutTeam(ShowUI.GetIdFromUser());
                            break;
                        case "23":
                            ShowUI.PutUser(ShowUI.GetIdFromUser());
                            break;
                        case "24":
                            ShowUI.GetAllUncompletedTasksByUserId(ShowUI.GetIdFromUser());
                            break;
                        case "25":
                            Timer timer = new Timer();
                            timer.Elapsed += ShowUI.MarkRandomTaskWithDelay;
                            timer.Interval = 1000;
                            timer.Start();
                            break;
                        case "exit":
                            return;
                        case "?":
                            ShowUI.GetCommandMenu();
                            break;
                    }
                    Console.WriteLine();
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message + "\n");
                }
            }
        }
    }
}
