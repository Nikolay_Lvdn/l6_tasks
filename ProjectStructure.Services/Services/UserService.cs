﻿using AutoMapper;
using ProjectStructure.Business.DTO;
using ProjectStructure.Business.Interfaces;
using ProjectStructure.DataAccess.Entities;
using ProjectStructure.DataAccess.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Task = System.Threading.Tasks.Task;
using TaskEntity = ProjectStructure.DataAccess.Entities.Task;

namespace ProjectStructure.Business.Services
{
    public class UserService : IUserService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        public UserService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task CreateAsync(UserDTO userDTO)
        {
            if (userDTO == null)
                throw new Exception("Wrong request body");

            var userEntity = _mapper.Map<User>(userDTO);

            userEntity.RegisteredAt = DateTime.Now;

            if (userEntity.Email != null && !System.Net.Mail.MailAddress.TryCreate(userEntity.Email, out System.Net.Mail.MailAddress result))
                throw new ArgumentException("Invalid Email");

            if ((await _unitOfWork.GetUserRepository.GetAsync()).Select(user => user.Email).Contains(userDTO.Email))
                throw new ArgumentException("User with such email is already exists");

            await _unitOfWork.GetUserRepository.CreateAsync(userEntity);
            await _unitOfWork.SaveChangesAsync();
        }

        public async Task<IList<UserDTO>> GetAllAsync()
        {
            return _mapper.Map<IList<User>, IList<UserDTO>>(await _unitOfWork.GetUserRepository.GetAsync());
        }

        public async Task UpdateAsync(UserDTO userDTO)
        {
            var userToUpdate = (await _unitOfWork.GetUserRepository.GetAsync()).FirstOrDefault(user => user.Id == userDTO.Id);

            if (userToUpdate == null)
                throw new Exception("No such user");

            if (userDTO.Email != null && !System.Net.Mail.MailAddress.TryCreate(userDTO.Email, out System.Net.Mail.MailAddress result))
                throw new ArgumentException("Invalid Email");

            if ((await _unitOfWork.GetUserRepository.GetAsync())
                .Where(user => user.Id != userToUpdate.Id)
                .Select(user => user.Email)
                .Contains(userDTO.Email))
                throw new ArgumentException("User with such email is already exists");

            userToUpdate.TeamId = userDTO.TeamId;
            userToUpdate.FirstName = userDTO.FirstName;
            userToUpdate.LastName = userDTO.LastName;
            userToUpdate.Email = userDTO.Email;
            userToUpdate.BirthDay = userDTO.BirthDay;
            await _unitOfWork.GetUserRepository.UpdateAsync(userToUpdate);
            await _unitOfWork.SaveChangesAsync();
        }

        public async Task DeleteAsync(int id)
        {
            var userToDelete = (await _unitOfWork.GetUserRepository.GetAsync()).FirstOrDefault(user => user.Id == id);

            if (userToDelete == null)
                throw new Exception("No such user");

            await _unitOfWork.GetUserRepository.DeleteAsync(id);
            await _unitOfWork.SaveChangesAsync();
        }

        public async Task<IList<Query5DTO>> GetQuery5()
        {
            var tasks = await _unitOfWork.GetTaskRepository.GetAsync();
            var users = (await _unitOfWork.GetUserRepository.GetAsync()).OrderBy(user => user.FirstName);

            if (!tasks.Any())
                throw new ArgumentNullException("There are no such tasks");

            return await Task.WhenAll(users.Select(MakeQuery5Selection));
        }

        private async Task<Query5DTO> MakeQuery5Selection(User user)
        {
            return await Task.Run(() =>
            {
                return new Query5DTO()
                {
                    User = _mapper.Map<UserDTO>(user),
                    Tasks = _mapper.Map<IList<TaskEntity>, IList<TaskDTO>>(user.Tasks.OrderByDescending(task => task.Name.Length).ToList())
                };
            });
        }

        public async Task<Query6DTO> GetQuery6(int id)
        {
            var users = await _unitOfWork.GetUserRepository.GetAsync();

            if (!users.Select(user => user.Id).Contains(id))
                throw new ArgumentNullException("There are no such user");

            var project = (await _unitOfWork.GetProjectRepository.GetAsync())
                .Where(project => project.AuthorId == id)
                .OrderBy(project => project.CreatedAt)
                .LastOrDefault();

            var user = users.FirstOrDefault(user => user.Id == id);

            var DataTimeNow = DateTime.Now;

            var resultQuery = new Query6DTO
            {
                User = _mapper.Map<UserDTO>(user),
                LastUserProject = _mapper.Map<ProjectDTO>(project),
                LastProjectTasksCount = project?.Tasks?.Count,
                CountOfIncompleteOrCanceledTasks = user.Tasks?.Count(task => task.FinishedAt == null || task.State == 3),
                LongestTask = _mapper.Map<TaskDTO>(user.Tasks?
                                    .OrderBy(task => task.FinishedAt != null ? task.FinishedAt - task.CreatedAt : DataTimeNow - task.CreatedAt)
                                    .LastOrDefault())
            };

            return resultQuery;
        }

    }
}
