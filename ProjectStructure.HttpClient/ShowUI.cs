﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using Newtonsoft.Json;
using ProjectStructure.Business.DTO;
using ProjectStructure.ServiceCommunication;
using ProjectStructure.WebAPI.Models;

namespace ProjectStructure.Show
{
    public static class ShowUI
    {
        #region Utilities
        public static void GetCommandMenu()
        {
            Console.WriteLine("1 - query1");
            Console.WriteLine("2 - query2");
            Console.WriteLine("3 - query3");
            Console.WriteLine("4 - query4");
            Console.WriteLine("5 - query5");
            Console.WriteLine("6 - query6");
            Console.WriteLine("7 - query7");
            Console.WriteLine("8 - Display all projects.");
            Console.WriteLine("9 - Display all Tasks.");
            Console.WriteLine("10 - Display all Teams.");
            Console.WriteLine("11 - Display all Users.");
            Console.WriteLine("12 - Delete Project.");
            Console.WriteLine("13 - Delete Task.");
            Console.WriteLine("14 - Delete Team.");
            Console.WriteLine("15 - Delete User.");
            Console.WriteLine("16 - Post Project.");
            Console.WriteLine("17 - Post Task.");
            Console.WriteLine("18 - Post Team.");
            Console.WriteLine("19 - Post User.");
            Console.WriteLine("20 - Put Project.");
            Console.WriteLine("21 - Put Task.");
            Console.WriteLine("22 - Put Team.");
            Console.WriteLine("23 - Put User.");
            Console.WriteLine("24 - query8");
            Console.WriteLine("25 - MarkRandomTaskWithDelay");
            Console.WriteLine("exit - to exit the program");
        }
        static void SuccessMessage()
        {
            Console.WriteLine("Success");
        }
        static void PrintToConsole<T>(IList<T> list)
        {
            foreach (var element in list)
            {
                Console.WriteLine(element.ToString());
            }
        }
        public static int GetIdFromUser(string entity = null)
        {
            int id;
            Console.WriteLine($"Enter {entity} Id");
            id = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine();
            return id;
        }
        public static string GetTextFromUser(string entity)
        {
            Console.WriteLine($"Enter {entity}");
            string text = Console.ReadLine();
            Console.WriteLine();
            return text;
        }
        public static int GetIntFromUser(string entity, int from, int to)
        {
            int value;
            Console.WriteLine($"Enter {entity}");
            value = Convert.ToInt32(Console.ReadLine());
            while (!(from <= value && to >= value))
            {
                Console.WriteLine($"Wrong {entity}");
                Console.WriteLine($"Enter {entity}");
                value = Convert.ToInt32(Console.ReadLine());
            }
            Console.WriteLine();
            return value;
        }
        #endregion

        #region Get
        public static async Task GetCountOfTasksOfProjectsByUserId(int id)
        {
            var result = await Endpoints.GetCountOfTasksOfProjectsByUserId(id);

            PrintToConsole(result);
        }

        public static async Task GetTasksByUserIdWithShortName(int id)
        {
            var result = await Endpoints.GetTasksByUserIdWithShortName(id);

            PrintToConsole(result);
        }

        public static async Task GetFinishedTaskByUserIdInThisYear(int id)
        {
            var result = await Endpoints.GetFinishedTaskByUserIdInThisYear(id);

            PrintToConsole(result);
        }

        public static async Task GetTeamsWithUsersOlderThan9()
        {
            var result = await Endpoints.GetTeamsWithUsersOlderThan9();

            PrintToConsole(result);
        }

        public static async Task GetOrderedUsersAndTasks()
        {
            var result = await Endpoints.GetOrderedUsersAndTasks();

            PrintToConsole(result);
        }

        public static async Task GetInfoAboutTasksByUserId(int id)
        {
            var result = await Endpoints.GetInfoAboutTasksByUserId(id);

            Console.WriteLine(result.ToString());
        }

        public static async Task GetInfoAboutProjects()
        {
            var result = await Endpoints.GetInfoAboutProjects();

            PrintToConsole(result);
        }

        public static async Task GetAllUncompletedTasksByUserId(int id)
        {
            var result = await Endpoints.GetAllUncompletedTasksByUserId(id);

            PrintToConsole(result);
        }

        public static async Task GetProjects()
        {
            List<GettingProjectView> projects = (List<GettingProjectView>)await Endpoints.GetProjects();

            PrintToConsole(projects);
        }

        public static async Task GetTasks()
        {
            List<GettingTaskView> tasks = (List<GettingTaskView>)await Endpoints.GetTasks();

            PrintToConsole(tasks);
        }

        public static async Task GetTeams()
        {
            List<GettingTeamView> teams = (List<GettingTeamView>)await Endpoints.GetTeams();

            PrintToConsole(teams);
        }

        public static async Task GetUsers()
        {
            List<GettingUserView> users = (List<GettingUserView>)await Endpoints.GetUsers();

            PrintToConsole(users);
        }
        #endregion

        #region Delete
        public static async Task DeleteProject(int id)
        {
            await Endpoints.DeleteProject(id);
            SuccessMessage();
        }
        public static async Task DeleteTask(int id)
        {
            await Endpoints.DeleteTask(id);
            SuccessMessage();
        }
        public static async Task DeleteTeam(int id)
        {
            await Endpoints.DeleteTeam(id);
            SuccessMessage();
        }
        public static async Task DeleteUser(int id)
        {
            await Endpoints.DeleteUser(id);
            SuccessMessage();
        }
        #endregion

        #region Post
        public static async Task PostProject()
        {
            ProjectView project = new ProjectView
            {
                AuthorId = GetIdFromUser("author"),
                TeamId = GetIdFromUser("team"),
                Name = GetTextFromUser("Name"),
                Description = GetTextFromUser("Description"),
                Deadline = new DateTime(GetIntFromUser("Year", DateTime.Now.Year, DateTime.Now.Year + 5),
                        GetIntFromUser("Month", 1, 12), GetIntFromUser("Day", 1, 31))
            };
            await Endpoints.PostProject(project);
            SuccessMessage();
        }
        public static async Task PostTask()
        {
            TaskView task = new TaskView
            {
                ProjectId = GetIdFromUser("Project"),
                PerformerId = GetIdFromUser("Performer"),
                Name = GetTextFromUser("Name"),
                Description = GetTextFromUser("Description"),
                State = GetIntFromUser("State", 0, 3)
            };
            await Endpoints.PostTask(task);
            SuccessMessage();
        }
        public static async Task PostTeam()
        {
            TeamView team = new TeamView
            {
                Name = GetTextFromUser("Name")
            };
            await Endpoints.PostTeam(team);
            SuccessMessage();
        }
        public static async Task PostUser()
        {
            UserView user = new UserView
            {
                TeamId = GetIdFromUser("Team"),
                FirstName = GetTextFromUser("First Name"),
                LastName = GetTextFromUser("Last Name"),
                Email = GetTextFromUser("Email"),
                BirthDay = new DateTime(GetIntFromUser("Year", DateTime.Now.Year - 40, DateTime.Now.Year),
                        GetIntFromUser("Month", 1, 12), GetIntFromUser("Day", 1, 31))
            };
            await Endpoints.PostUser(user);
            SuccessMessage();
        }
        #endregion

        #region Put
        public static async void MarkRandomTaskWithDelay(object sender, ElapsedEventArgs e)
        {
            try
            {
                var markedTaskId = await Endpoints.MarkRandomTaskWithDelay(1000);
                Console.WriteLine("Marked id is: " + markedTaskId);
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Marking failed with message: \"{ex.Message}\"");
            }
        }
        public static async Task PutProject(int id)
        {
            ProjectView project = new ProjectView
            {
                AuthorId = GetIdFromUser("author"),
                TeamId = GetIdFromUser("team"),
                Name = GetTextFromUser("Name"),
                Description = GetTextFromUser("Description"),
                Deadline = new DateTime(GetIntFromUser("Year", DateTime.Now.Year, DateTime.Now.Year + 5),
                        GetIntFromUser("Month", 1, 12), GetIntFromUser("Day", 1, 31))
            };
            await Endpoints.PutProject(id, project);
            SuccessMessage();
        }
        public static async Task PutTask(int id)
        {
            TaskView task = new TaskView
            {
                ProjectId = GetIdFromUser("Project"),
                PerformerId = GetIdFromUser("Performer"),
                Name = GetTextFromUser("Name"),
                Description = GetTextFromUser("Description"),
                State = GetIntFromUser("State", 0, 3)
            };
            await Endpoints.PutTask(id, task);
            SuccessMessage();
        }
        public static async Task PutTeam(int id)
        {
            TeamView team = new TeamView
            {
                Name = GetTextFromUser("Name")
            };
            await Endpoints.PutTeam(id, team);
            SuccessMessage();
        }
        public static async Task PutUser(int id)
        {
            UserView team = new UserView
            {
                TeamId = GetIdFromUser("Team"),
                FirstName = GetTextFromUser("First Name"),
                LastName = GetTextFromUser("Last Name"),
                Email = GetTextFromUser("Email"),
                BirthDay = new DateTime(GetIntFromUser("Year", DateTime.Now.Year - 40, DateTime.Now.Year),
                        GetIntFromUser("Month", 1, 12), GetIntFromUser("Day", 1, 31))
            };
            await Endpoints.PutUser(id, team);
            SuccessMessage();
        }
        #endregion
    }
}
