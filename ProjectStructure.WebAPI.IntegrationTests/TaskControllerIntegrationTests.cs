using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using ProjectStructure.DataAccess;
using ProjectStructure.WebAPI.Models;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Text;
using Xunit;

namespace ProjectStructure.WebAPI.IntegrationTests
{
    public class TaskControllerIntegrationTests : IClassFixture<CustomWebApplicationFactory<Startup>>, IDisposable
    {
        private readonly HttpClient _client;
        private CompanyDbContext _context;

        public TaskControllerIntegrationTests(CustomWebApplicationFactory<Startup> factory)
        {
            _client = factory.CreateClient();

            factory.DBName = "DB" + GetType().Name;

            var scopeFactory = factory.Services.GetService<IServiceScopeFactory>();
            var scope = scopeFactory.CreateScope();
            _context = scope.ServiceProvider.GetService<CompanyDbContext>();
            _context.Database.EnsureCreated();
        }

        public void Dispose()
        {
            _context.Database.EnsureDeleted();
        }

        [Fact]
        public async void CreateTaskResponceWithCode201_ThanDeleteResponceWithCode204()
        {
            var httpPostResponce = await _client.PostTaskAsync("Test");

            var stringPostResponce = await httpPostResponce.Content.ReadAsStringAsync();
            var createdTask = JsonConvert.DeserializeObject<TaskView>(stringPostResponce);

            Assert.Equal(HttpStatusCode.Created, httpPostResponce.StatusCode);
            Assert.Equal("Test", createdTask.Name);

            var httpDeleteResponce = await _client.DeleteAsync("api/Task/1");

            Assert.Equal(HttpStatusCode.NoContent, httpDeleteResponce.StatusCode);
        }

        [Fact]
        public async void DeleteTask_ThatDontExists_ThenDeleteResponceWithCode404()
        {
            var httpDeleteResponce = await _client.DeleteAsync("api/Task/1");

            Assert.Equal(HttpStatusCode.NotFound, httpDeleteResponce.StatusCode);
        }

        [Fact]
        public async void Query8_NoUserWithRequestedId_ThanResponceWithCode404()
        {
            var httpResponce = await _client.GetAsync("api/Task/q8/?id=1");

            Assert.Equal(HttpStatusCode.NotFound, httpResponce.StatusCode);
        }

        [Fact]
        public async void Query8_WhenNoUncompletedTasks_ThanResponceWithCode404()
        {
            await _client.PostUserAsync("Test");

            await _client.PostTaskAsync(name: "Test1", performerId: 1, state: 2);

            await _client.PostTaskAsync(name: "Test3", performerId: 1, state: 3);

            var httpResponce = await _client.GetAsync("api/Task/q8/?id=1");

            Assert.Equal(HttpStatusCode.NotFound, httpResponce.StatusCode);
        }

        [Fact]
        public async void Query8_When2UncompletedTasks_ThanResponceWithCode200AndTwoElements()
        {
            await _client.PostUserAsync("Test");

            await _client.PostTaskAsync(name: "Test1", performerId: 1, state: 0);

            await _client.PostTaskAsync(name: "Test3", performerId: 1, state: 1);

            var httpResponce = await _client.GetAsync("api/Task/q8/?id=1");
            var stringResponce = await httpResponce.Content.ReadAsStringAsync();
            var tasks = JsonConvert.DeserializeObject<List<GettingTaskView>>(stringResponce);

            Assert.Equal(2, tasks.Count);
        }
    }
}
