using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using ProjectStructure.DataAccess;
using ProjectStructure.WebAPI.Models;
using System;
using System.Net;
using System.Net.Http;
using System.Text;
using Xunit;

namespace ProjectStructure.WebAPI.IntegrationTests
{
    public class TeamControllerIntegrationTests : IClassFixture<CustomWebApplicationFactory<Startup>>, IDisposable
    {
        private readonly HttpClient _client;
        private CompanyDbContext _context;

        public TeamControllerIntegrationTests(CustomWebApplicationFactory<Startup> factory)
        {
            _client = factory.CreateClient();

            factory.DBName = "DB" + GetType().Name;

            var scopeFactory = factory.Services.GetService<IServiceScopeFactory>();
            var scope = scopeFactory.CreateScope();
            _context = scope.ServiceProvider.GetService<CompanyDbContext>();
            _context.Database.EnsureCreated();
        }

        public void Dispose()
        {
            _context.Database.EnsureDeleted();
        }

        [Fact]
        public async void CreateTeam_ThanResponceWithCode201()
        {
            var httpResponce = await _client.PostTeamAsync("Test");

            var stringResponce = await httpResponce.Content.ReadAsStringAsync();
            var createdTeam = JsonConvert.DeserializeObject<TeamView>(stringResponce);

            Assert.Equal(HttpStatusCode.Created, httpResponce.StatusCode);
            Assert.Equal("Test",createdTeam.Name);
        }

        [Fact]
        public async void CreateTeam_ThatAlreadyExists_ThenResponceWithCode400()
        {
            await _client.PostTeamAsync("RepeatedTeam");
            var httpResponce = await _client.PostTeamAsync("RepeatedTeam");

            Assert.Equal(HttpStatusCode.BadRequest, httpResponce.StatusCode);
        }


    }
}
